package Kerving.Collections;

public class Products {

        private String article;
        private double price;

        public String getArticle() {
            return article;
        }

        public void setArticle(String article) {
            this.article = article;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String toString() {
            return "Article: " + article + " Price: " + price;
        }
    }
