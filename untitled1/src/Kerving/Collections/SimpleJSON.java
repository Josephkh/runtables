package Kerving.Collections;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public class SimpleJSON {

        public static String productsToJSON(Products products) {

            ObjectMapper mapper = new ObjectMapper();
            String s = "";

            try {
                s = mapper.writeValueAsString(products);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return s;
        }

        public static Products JSONToProducts(String s) throws IOException {

            ObjectMapper mapper = new ObjectMapper();
            Products products = null;

            try {
                products = mapper.readValue(s, Products.class);
            }
            catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }

            return products;
        }

        public static void main(String[] args) throws IOException {

            Products product1 = new Products();
            product1.setArticle("Banana");
            product1.setPrice(2.0);
            Products product3 = new Products();
            product3.setArticle("Mango");
            product3.setPrice(2.5);

            String json = SimpleJSON.productsToJSON(product1);
            String json1 = SimpleJSON.productsToJSON(product3);
            System.out.println(json);
            System.out.println(json1);

            Products product2 = SimpleJSON.JSONToProducts(json);
            System.out.println(product2);
            System.out.println(product3);
        }

    }
