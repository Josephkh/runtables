package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class weatherexecution {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        weatherprojection wp1 = new weatherprojection("The storm", 5, 500);
        weatherprojection wp2 = new weatherprojection("The rain", 4, 300);
        weatherprojection wp3 = new weatherprojection("The hail", 3, 200);
        weatherprojection wp4 = new weatherprojection("The earthquake", 2, 100);
        weatherprojection wp5 = new weatherprojection("The thunder", 1, 50);

        myService.execute(wp1);
        myService.execute(wp2);
        myService.execute(wp3);
        myService.execute(wp4);
        myService.execute(wp5);

        myService.shutdown();
    }
}


