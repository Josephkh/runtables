package com.company;

import java.util.Random;

public class weatherprojection implements Runnable {

    private String name;
    private int number;
    private int action;
    private int rand;

    public weatherprojection(String name, int number, int action) {

        this.name = name;
        this.number = number;
        this.action = action;

        Random random = new Random();
        this.rand = random.nextInt(500);
    }

    public void run() {
        System.out.println("\n\nExecuting with these parameters: Name =" + name + " Number = "
                + number + " Action = " + action + " Rand Num = " + rand + "\n\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is raging. ");
                try {
                    Thread.sleep(action);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + name + " has stopped.\n\n");
    }
}

