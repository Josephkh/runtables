package Final;

import java.util.Scanner;

public class Resolve {

    public static void main(String[] Strings) {

        Scanner input = new Scanner(System.in);

        System.out.print("Input A: ");
        double A = input.nextDouble();
        System.out.print("Input B: ");
        double B = input.nextDouble();
        System.out.print("Input C: ");
        double C = input.nextDouble();

        double result = B * B - 4.0 * A * C;
        System.out.println(result);

        if (result > 0.0) {
            double x1 = (-B + Math.pow(result, 0.5)) / (2.0 * A);
            double x2 = (-B - Math.pow(result, 0.5)) / (2.0 * A);
            System.out.println("The roots are X1 = " + x1 + " and x2 = " + x2);
        } else if (result == 0.0) {
            double x1 = -B / (2.0 * A);
            System.out.println("The root is x2 = x1 = " + x1);
        } else {
            System.out.println("The equation has no real roots.");
        }

    }
}
