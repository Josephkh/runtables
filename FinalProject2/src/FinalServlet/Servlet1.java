package FinalServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;

@WebServlet(name = "Servlet1", urlPatterns ={"/Servlet1"})
public class Servlet1 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String A = request.getParameter("A");
        String B = request.getParameter("B");
        String C = request.getParameter("C");

        out.println("<h1>Display your Login Information</h1>");
        out.println("<p> A: " + A + "</p>");
        out.println("<p> B: " + B + "</p>");
        out.println("<p> C: " + C + "</p>");

        out.println("</body></html>");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");

    }
}
