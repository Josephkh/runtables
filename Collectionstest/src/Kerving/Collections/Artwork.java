package Kerving.Collections;

public class Artwork {

    private String name;
    private String artist;

    public Artwork(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

   public String toString() {
        return "Name: " + name + "  " + "Artist: " + artist;
    }
}
