package Kerving.Collections;

import java.util.*;

public class BasicCollection {

    public static void main(String[] args) {
        System.out.println("-- The classroom list --");
        List list = new ArrayList();
        list.add("books");
        list.add("Students");
        list.add("computers");
        list.add("Teacher");
        list.add("students");
        list.add("pen");

        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("-- The classroom Set --");
        Set set = new TreeSet();
        set.add("books");
        set.add("Students");
        set.add("computers");
        set.add("Teacher");
        set.add("students");
        set.add("pen");

        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("-- The classroom Queue --");
        Queue queue = new PriorityQueue();
        queue.add("books");
        queue.add("Students");
        queue.add("computers");
        queue.add("Teacher");
        queue.add("students");
        queue.add("pen");

        Iterator iterator =queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }

        System.out.println("-- The classroom Map --");
        Map map = new HashMap();
        map.put(1,"books");
        map.put(2,"Students");
        map.put(3,"computers");
        map.put(4,"Teacher");
        map.put(5,"students");
        map.put(6,"pen");

        for (int i = 1; i < 6; i++) {
            String show = (String)map.get(i);
            System.out.println(show);
        }

        System.out.println("List of Class artworks");
        List<Artwork> myList = new LinkedList<Artwork>();
        myList.add(new Artwork( "David  ", "Gian L. Bernini"));
        myList.add(new Artwork( "Pieta  ", "Michelangelo"));
        myList.add(new Artwork( "Velata ", "Raphael"));
        myList.add(new Artwork( "M. Lisa", "Leonardo Da Vinci"));

        for (Artwork artwork : myList) {
            System.out.println(artwork);
        }
        
    }

}
